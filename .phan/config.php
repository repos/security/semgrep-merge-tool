<?php

$cfg = [];

$cfg["backward_compatibility_checks"] = false;
$cfg["quick_mode"] = false;
$cfg["analyze_signature_compatibility"] = true;
$cfg["ignore_undeclared_variables_in_global_scope"] = false;
$cfg["read_type_annotations"] = true;
$cfg["disable_suppression"] = false;
$cfg["dump_ast"] = false;
$cfg["processes"] = 1;
$cfg["markdown_issue_messages"] = false;
$cfg["generic_types_enabled"] = true;

$cfg["plugins"] = [
	"PregRegexCheckerPlugin",
	"UnusedSuppressionPlugin",
	"DuplicateExpressionPlugin",
	"LoopVariableReusePlugin",
	"RedundantAssignmentPlugin",
	"UnreachableCodePlugin",
	"SimplifyExpressionPlugin",
	"DuplicateArrayKeyPlugin",
	"UseReturnValuePlugin",
	"AddNeverReturnTypePlugin",
	"vendor/mediawiki/phan-taint-check-plugin/GenericSecurityCheckPlugin.php"
];

$cfg["file_list"] = [
	"config.example.php",
	"index.php"
];

$cfg["directory_list"] = [
	"src",
	"tests",
	"vendor"
];

$cfg["exclude_analysis_directory_list"] = [
	"./phan/stubs",
	"vendor",
];

// phpcs:ignore
$cfg["exclude_file_regex"] = "@vendor/((composer/installers|php-parallel-lint/php-console-color|php-parallel-lint/php-console-highlighter|php-parallel-lint/php-parallel-lint|mediawiki/mediawiki-codesniffer|microsoft/tolerant-php-parser|phan/phan|phpunit/php-code-coverage|squizlabs/php_codesniffer|[^/]+/[^/]+/\.phan)|.*/[Tt]ests?)/@";

$cfg["minimum_severity"] = 0;
$cfg["allow_missing_properties"] = false;
$cfg["null_casts_as_any_type"] = false;
$cfg["scalar_implicit_cast"] = false;
$cfg["dead_code_detection"] = false;
$cfg["dead_code_detection_prefer_false_negative"] = true;

$cfg["suppress_issue_types"] = [
	"PhanDeprecatedFunction",
	"PhanDeprecatedClass",
	"PhanDeprecatedClassConstant",
	"PhanDeprecatedFunctionInternal",
	"PhanDeprecatedInterface",
	"PhanDeprecatedProperty",
	"PhanDeprecatedTrait",
	"PhanUnreferencedUseNormal",
	"PhanUnreferencedUseFunction",
	"PhanUnreferencedUseConstant",
	"PhanDuplicateUseNormal",
	"PhanDuplicateUseFunction",
	"PhanDuplicateUseConstant",
	"PhanUseNormalNoEffect",
	"PhanUseNormalNamespacedNoEffect",
	"PhanUseFunctionNoEffect",
	"PhanUseConstantNoEffect",
	"PhanDeprecatedCaseInsensitiveDefine",
	"PhanAccessClassConstantInternal",
	"PhanAccessClassInternal",
	"PhanAccessConstantInternal",
	"PhanAccessMethodInternal",
	"PhanAccessPropertyInternal",
	"PhanParamNameIndicatingUnused",
	"PhanParamNameIndicatingUnusedInClosure",
	"PhanProvidingUnusedParameter",
	"PhanCompatibleSerializeInterfaceDeprecated",
	"PhanPluginMixedKeyNoKey",
	"SecurityCheck-LikelyFalsePositive",
	"SecurityCheck-PHPSerializeInjection"
];

$cfg["enable_class_alias_support"] = true;
$cfg["redundant_condition_detection"] = true;
$cfg["minimum_target_php_version"] = "8.2";
$cfg["target_php_version"] = "8.2";

return $cfg;
