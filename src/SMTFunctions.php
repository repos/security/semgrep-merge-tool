<?php

/*
 * Copyright 2022 sbassett@wikimedia.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

namespace Wikimedia\SemgrepMergeTool;

trait SMTFunctions {
	/**
	 * @param string $url
	 * @return string
	 */
	private static function gitPathFromRepoURL( string $url ) {
		// tries to find git path after scheme + domain
		// should leave non-.git file paths alone
		$path = parse_url( $url, PHP_URL_PATH );
		$path = pathinfo( $path )['dirname'] .
			DIRECTORY_SEPARATOR .
			pathinfo( $path )['filename'];
		if ( strpos( $path, DIRECTORY_SEPARATOR ) == 0 ) {
			$path = substr( $path, 1 );
		}
		return $path;
	}

	/**
	 * @param string $data
	 * @return string
	 */
	private static function shortHash( string $data ) {
		return substr( hash( 'sha256', $data ), 0, 16 );
	}

	/**
	 * @param string $data
	 * @return string
	 */
	private static function longHash( string $data ) {
		return hash( 'sha256', $data );
	}
}
