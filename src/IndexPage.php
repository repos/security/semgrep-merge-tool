<?php

/*
 * Copyright 2022 sbassett@wikimedia.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

namespace Wikimedia\SemgrepMergeTool;

use Exception;

require_once __DIR__ . '/../vendor/autoload.php';

class IndexPage {
	use SMTFunctions;

	/**
	 * @param array $config
	 * @return null
	 * @throws Exception
	 */
	public static function render( array $config ) {
		if ( !array_key_exists( 'smt_template_dir', $config )
		|| !is_dir( $config['smt_template_dir'] ) ) {
			throw new Exception( 'Incomplete configuration.' );
		}
		$loader = new \Twig\Loader\FilesystemLoader( $config['smt_template_dir'] );
		$twig = new \Twig\Environment( $loader );
		$template = $twig->load( 'index.twig.html' );
		if ( !isset( $_SERVER ) ) {
			return null;
		}
		echo $template->render(
			[
				'base_url' => "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'],
				'repos' => self::addCacheInfo(
					self::prepareRepoData( $config['smt_rule_repos'] ),
					$config['smt_cache_dir']
				),
				'policies' => self::addCacheInfo(
					$config['smt_policies'],
					$config['smt_cache_dir']
				)
			]
		);
	}

	/**
	 * @param array $rule_repos
	 * @return array
	 */
	private static function prepareRepoData( array $rule_repos ) {
		$arr = [];
		foreach ( $rule_repos as $dirkey => $data ) {
			$arr[ $dirkey ]['subdirs'] = $data['subdirs'] ?? [];
		}
		return $arr;
	}

	/**
	 * @param array $dir_data
	 * @param string $cache_path
	 * @return array
	 */
	private static function addCacheInfo(
		array $dir_data,
		string $cache_path
	) {
		$cache_info_added = [];
		foreach ( $dir_data as $key => $val ) {
			$subdirs = $val;
			if ( array_key_exists( 'subdirs', $val ) ) {
				$subdirs = $val['subdirs'];
			}

			$tmp = [];
			$tmp['cached'] = YamlManager::cacheFileExists(
				$key,
				$cache_path
			);
			$cached_file_exists = '';
			foreach ( $subdirs as $subdir ) {
				$full_path = $key . DIRECTORY_SEPARATOR . $subdir;
				$cached_file_exists = YamlManager::cacheFileExists(
					$full_path,
					$cache_path
				);
				$tmp['subdirs'][ $subdir ] = $cached_file_exists;
			}
			$cache_info_added[ $key ] = $tmp;
		}
		return $cache_info_added;
	}
}
