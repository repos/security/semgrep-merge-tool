<?php

/**
 * Copyright 2022 sbassett@wikimedia.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Maitenance script/cli largely for performing:
 *  1) shallow clones/deletes of relevant semgrep rules repos
 *  NOTE: expected to be run within util directory
 */

namespace Wikimedia\SemgrepMergeTool;

/* require config and dependencies */
require_once __DIR__ . '/../vendor/autoload.php';

use Exception;

class SMTCliClass {
	/**
	 * @param array|null $config
	 * @return void
	 * @throws Exception
	 */
	public static function run( ?array $config = null ) {
		if ( $config == null ) {
			$config = require __DIR__ . '/../config.php';
		}
		if ( !array_key_exists( 'smt_rule_repos', $config )
			|| !array_key_exists( 'smt_cache_dir', $config ) ) {
			throw new Exception( 'Configuration appears to be invalid.' );
		}
		$passed_action = self::processCliArgs();
		switch ( $passed_action ) {
			case "repos get":
				self::reposGet( $config );
				break;
			case 'clear hashes':
				self::clearCacheHashes( $config['smt_cache_dir'] );
				break;
			case "help":
				// @phan-suppress-next-line SecurityCheck-XSS CLI context
				self::displayHelp( $passed_action );
				break;
			default:
				// @phan-suppress-next-line SecurityCheck-XSS CLI context
				self::displayHelp( $passed_action );
				break;
		}
	}

	/**
	 * @param array $config
	 * @return void
	 */
	private static function reposGet( array $config ) {
		if ( !is_array( $config['smt_rule_repos'] ) ) {
			throw new Exception( "ERROR: 'smt_rules_repos' config variable is not an array." );
		}
		foreach ( $config['smt_rule_repos'] as $dirkey => $data ) {
			if ( preg_match( "/^(http(s)?:\/\/(.+)\.git)$/", $data['repo'] ) ) {
				$cache_dir = $config['smt_cache_dir'];
				$local_repo_path =
					$cache_dir . DIRECTORY_SEPARATOR . $dirkey;
				$version = $data['version'];

				// force purge of old repo dir every time
				if ( is_dir( $local_repo_path ) ) {
					$cmd = "rm -rf {$local_repo_path}";
					$out = null;
					$ret = null;
					// phpcs:ignore
					exec( $cmd, $out, $ret );
					if ( $ret == 0 ) {
						echo "STATUS: existing cache dir '{$local_repo_path}' deleted.\n";
					}
				}

				// clone repos
				$cmd = "git clone --depth=1 {$data['repo']} {$local_repo_path} 2>&1 " .
						"&& cd {$local_repo_path} " .
						"&& git fetch --all --tags " .
						"&& git checkout {$version}";
				$out = null;
				$ret = null;
				// phpcs:ignore
				exec( $cmd, $out, $ret );

				if ( $ret == 0 ) {
					echo "SUCCESS: {$data['repo']} was locally cloned.\n";
				} else {
					throw new Exception( "ERROR: {$data['repo']} was NOT locally cloned." );
				}
			} else {
				throw new Exception( "ERROR: {$data['repo']} does NOT appear to be a valid git repository." );
			}
		}
	}

	/**
	 * @param string $cache_path
	 * @return void
	 */
	private static function clearCacheHashes( $cache_path = './cache' ) {
		$cmd = "find {$cache_path} -maxdepth 1 -type f -delete";
		$out = null;
		$ret = null;
		// phpcs:ignore
		exec( $cmd, $out, $ret );

		if ( $ret == 0 ) {
			echo "SUCCESS: hash files in cache have been cleared.\n";
		} else {
			echo "ERROR: hash files in cache HAVE NOT been cleared.\n";
		}
	}

	/**
	 * @return string
	 */
	private static function processCliArgs() {
		global $argv;
		$opts = array_slice( $argv, 1 );
		$opts_str = implode( ' ', $opts );
		return $opts_str;
	}

	/**
	 * @param string $action
	 * @return void
	 */
	private static function displayHelp( string $action ) {
		if ( !empty( $action ) && $action !== "help" ) {
			// phpcs:ignore
			$safe_action = escapeshellarg( $action );
			echo "Unrecognized action '{$action}'\n\n";
		} elseif ( $action !== "help" ) {
			echo "No action passed, which is required.\n\n";
		}
		echo "Usage: ./SMTCli.php {action}\n\n";
		echo "Actions:\n\n";
		echo "help          Show this help output\n";
		echo "repos get     Get semgrep rule repos, as defined in config\n";
		echo "clear hashes  Delete hash files within cache (repo dirs remain)";
	}
}
