<?php

/*
 * Copyright 2022 sbassett@wikimedia.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

namespace Wikimedia\SemgrepMergeTool;

use Symfony\Component\Yaml\Yaml;

class YamlManager {
	use SMTFunctions;

	private const Q_TYPE_REPO_DIR = 1;
	private const Q_TYPE_REPO_SUB_DIR = 2;
	private const Q_TYPE_REPO_FILE = 3;
	private const Q_TYPE_SHORT_HASH = 4;
	private const Q_TYPE_POLICY = 5;

	/**
	 * Primary public entrypoint for class,
	 * renders requested yaml rules and policies
	 *
	 * @param array $config
	 * @param string $query
	 * @return void
	 */
	public static function render( array $config, string $query ) {
		$cache_item = self::createAndGetCachedItem(
			$query,
			$config['smt_rule_repos'],
			$config['smt_policies'],
			$config['smt_exclude_rules'],
			$config['smt_cache_ttl'],
			$config['smt_cache_dir']
		);

		// set content-type header and echo pre-formatted yaml cache item
		header( "Content-type: text/x-yaml\r\n" );
		echo $cache_item;
	}

	/**
	 * Split (comma-separate) query variable 'q' for various
	 * file and directory types (see constants above)
	 *
	 * @param string $query
	 * @param array $rule_repos
	 * @param array $policies
	 * @param string $cache_dir
	 * @return array $types
	 */
	private static function sniffQueryForTypes(
		string $query,
		array $rule_repos,
		array $policies,
		string $cache_dir = './cache'
	) {
		$types = [];
		$items = [ $query ];
		if ( strpos( $query, ',' ) ) {
			$items = preg_split( "/,/", $query );
		}

		foreach ( $items as $val ) {
			// handle policies
			if ( array_key_exists( $val, $policies ) ) {
				$policy_dirs = $policies[ $val ];

				foreach ( $policy_dirs as $dir ) {
					$types[ $dir ] = self::searchCacheDirForType(
						$dir,
						$rule_repos,
						$policies,
						$cache_dir
					);
				}
			} else {
				// everything that isn't a policy
				$types[ $val ] = self::searchCacheDirForType(
					$val,
					$rule_repos,
					$policies,
					$cache_dir
				);
			}
		}

		return $types;
	}

	/**
	 * @param string $dir_or_file
	 * @param string $cache_path
	 * @return string
	 */
	public static function cacheFileExists(
		string $dir_or_file,
		string $cache_path = './cache'
	) {
		$ret = '';
		$hash_path = self::shortHash( $dir_or_file );
		if ( realpath( $cache_path . DIRECTORY_SEPARATOR . $hash_path ) ) {
			$ret = $hash_path;
		}
		return $ret;
	}

	/**
	 * Search the fs cache for files and dirs from various rule repos
	 * and determine what type they are (see constants above)
	 *
	 * @param string $path
	 * @param array $rule_repos
	 * @param array $policies
	 * @param string $cache_path
	 * @return int
	 */
	private static function searchCacheDirForType(
		string $path,
		array $rule_repos = [],
		array $policies = [],
		string $cache_path = './cache'
	) {
		$type = 0;
		$full_path = realpath( $cache_path . DIRECTORY_SEPARATOR . $path );

		$is_base_repo = false;
		foreach ( $rule_repos as $dirkey => $data ) {
			if ( $dirkey == self::gitPathFromRepoURL( $path ) ) {
				$is_base_repo = true;
			}
		}

		if ( in_array( $path, $policies ) ) {
			$type = self::Q_TYPE_POLICY;
		} elseif (
			preg_match( "/^[A-Za-z0-9]{0,16}$/", $path ) &&
			is_file( $full_path )
		) {
			$type = self::Q_TYPE_SHORT_HASH;
		} elseif ( $is_base_repo && is_dir( $full_path ) ) {
			$type = self::Q_TYPE_REPO_DIR;
		} elseif ( is_dir( $full_path ) ) {
			$type = self::Q_TYPE_REPO_SUB_DIR;
		} elseif ( is_file( $full_path ) ) {
			$type = self::Q_TYPE_REPO_FILE;
		}
		return $type;
	}

	/**
	 * Read all yaml file data from a given path
	 *
	 * @param string $path
	 * @param int $type
	 * @param string $cache_path
	 * @param array $rule_repos
	 * @param array $exclude_rules
	 * @return array
	 */
	private static function readYamlFiles(
		string $path,
		int $type,
		string $cache_path = './cache',
		array $rule_repos = [],
		array $exclude_rules = []
	) {
		$file_data = [];
		$full_path = realpath( $cache_path . DIRECTORY_SEPARATOR . $path );
		if ( !$full_path ) {
			// we should always have a valid cache path here
			throw new \InvalidArgumentException( "Invalid rule path '({$path})' specified." );
		}

		// determine file or dir type then read data
		if ( $type == self::Q_TYPE_REPO_DIR ) {
			$subdirs = $rule_repos[$path]['subdirs'] ?? [];
			foreach ( $subdirs as $dir ) {
				$subdir_path = $full_path . DIRECTORY_SEPARATOR . $dir;
				foreach ( self::getDirContents( $subdir_path ) as $file ) {
					$exclude_rule_path = $path . DIRECTORY_SEPARATOR . $dir;
					$yaml_data = file_get_contents( $file );
					if ( isset( $exclude_rules[$exclude_rule_path] ) ) {
						$yaml_data = self::removeYamlBlock(
							$yaml_data,
							'id',
							$exclude_rules[$exclude_rule_path] );
					}
					if ( $yaml_data != '' ) {
						$file_data[] = $yaml_data;
					}
				}
			}
		} elseif ( $type == self::Q_TYPE_REPO_SUB_DIR ) {
			foreach ( self::getDirContents( $full_path ) as $file ) {
				$yaml_data = file_get_contents( $file );
				if ( isset( $exclude_rules[$path] ) ) {
					$yaml_data = self::removeYamlBlock( $yaml_data, 'id', $exclude_rules[$path] );
				}
				if ( $yaml_data != '' ) {
					$file_data[] = $yaml_data;
				}
			}
		} elseif (
			$type == self::Q_TYPE_REPO_FILE ||
			$type == self::Q_TYPE_SHORT_HASH ||
			$type == self::Q_TYPE_POLICY
		) {
			$yaml_data = file_get_contents( $full_path );
			if ( $type == self::Q_TYPE_REPO_FILE ) {
				$yaml_data = self::removeYamlBlock(
					$yaml_data,
					'id',
					$exclude_rules[dirname( $path )]
				);
			}
			if ( $yaml_data != '' ) {
				$file_data[] = $yaml_data;
			}
		}

		return $file_data;
	}

	/**
	 * Remove a given block of yaml based upon a key/values lookup
	 * (assumes 1d linear block of semgrep-rule yaml, removes all rule data)
	 *
	 * @param string $yaml_data
	 * @param string $yaml_key
	 * @param array $yaml_values
	 * @return string
	 */
	private static function removeYamlBlock(
		string $yaml_data,
		string $yaml_key,
		array $yaml_values
	) {
		$yaml_data = Yaml::parse( $yaml_data );
		$yaml_data_itr = new \RecursiveIteratorIterator(
			new \RecursiveArrayIterator( $yaml_data )
		);

		$processed_yaml = '';
		$sub_array_data = [];
		foreach ( $yaml_data_itr as $sub ) {
			$sub_array = $yaml_data_itr->getSubIterator();
			$sub_array_data = iterator_to_array( $sub_array );
			if (
				array_key_exists( $yaml_key, $sub_array_data ) &&
				in_array( $sub_array_data[$yaml_key], $yaml_values )
			) {
				unset( $sub_array_data );
				break;
			}
		}
		// @phan-suppress-next-line PhanRedundantCondition $sub_array_data can be unset in foreach above
		if ( isset( $sub_array_data ) ) {
			$processed_yaml = [ "rules" => [ $sub_array_data ] ];
			$processed_yaml = Yaml::dump( $processed_yaml, 2, 4, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK );
		}
		return $processed_yaml;
	}

	/**
	 * Recursively read files and dirs in given $dir,
	 * filter found files on $file_ext_filter
	 * h/t stackoverflow.com/a/24784144
	 *
	 * @param string $dir
	 * @param array &$found_files
	 * @param array $file_ext_filter
	 * @return array
	 */
	private static function getDirContents(
		string $dir,
		array &$found_files = [],
		array $file_ext_filter = [ 'yaml', 'yml' ]
	) {
		$files = scandir( $dir );
		foreach ( $files as $k => $file ) {
			$path = realpath( $dir . DIRECTORY_SEPARATOR . $file );

			if ( is_file( $path ) ) {
				$file_ext = pathinfo( $file )['extension'] ?? '';
				if ( in_array( $file_ext, $file_ext_filter ) ) {
					$found_files[] = $path;
				}
			} elseif ( is_dir( $path ) && $file != "." && $file != ".." ) {
				self::getDirContents( $path, $found_files );
			}
		}
		return $found_files;
	}

	/**
	 * A very simple yaml-merging function
	 *
	 * @param array $yaml_data
	 * @return string
	 */
	private static function mergeYaml(
		array $yaml_data
	) {
		$merged_yaml = "rules:\n";
		foreach ( $yaml_data as $yaml ) {
			$data = Yaml::parse( $yaml );

			// extra validation of semgrep-style rule files
			$data_no_rules_key = $data;
			if ( array_key_exists( 'rules', $data ) ) {
				$data_no_rules_key = $data['rules'];
			}

			$merged_yaml .= Yaml::dump( $data_no_rules_key, 2, 4, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK );
		}

		// sanity check for edge cases, rule exclusions
		if ( $merged_yaml == "rules:\n" ) {
			$merged_yaml = '';
		}

		return $merged_yaml;
	}

	/**
	 * If a cache key (hashed query string) exists in the cache,
	 * return its contents.  If not, create it, and _then_ return
	 * its contents.
	 *
	 * @param string $query
	 * @param array $rule_repos
	 * @param array $policies
	 * @param array $exclude_rules
	 * @param int $cache_ttl
	 * @param string $cache_path
	 * @return string
	 */
	private static function createAndGetCachedItem(
		string $query,
		array $rule_repos = [],
		array $policies = [],
		array $exclude_rules = [],
		int $cache_ttl = 604800,
		string $cache_path = './cache'
	) {
		$yaml_data = '';

		// sort query params to reduce/optimize cache keys
		if ( strpos( $query, ',' ) ) {
			$items = preg_split( "/,/", $query );
			sort( $items );
			$query = implode( ',', $items );
		}

		$query_hash = self::shortHash( $query );
		$full_path = realpath( $cache_path . DIRECTORY_SEPARATOR . $query_hash );

		if (
			$full_path &&
			is_file( $full_path ) &&
			filemtime( $full_path ) > ( time() - $cache_ttl ) &&
			preg_match( "/[A-Za-z0-9]{0,16}/", $query_hash )
		) {
			// cache hit for short hash
			$yaml_data = file_get_contents( $full_path );
		} else {
			// no cache hit
			$file_paths_and_types = self::sniffQueryForTypes(
				$query,
				$rule_repos,
				$policies,
				$cache_path
			);

			$yaml_data = [];
			foreach ( $file_paths_and_types as $file_path => $file_type ) {
				$data = self::readYamlFiles(
					$file_path,
					$file_type,
					$cache_path,
					$rule_repos,
					$exclude_rules
				);
				$yaml_data = array_merge( $yaml_data, $data );
			}
			$yaml_data = self::mergeYaml( $yaml_data );

			// write yaml data to cache
			file_put_contents(
				$cache_path . DIRECTORY_SEPARATOR . $query_hash,
				$yaml_data,
				LOCK_EX
			);
		}

		return $yaml_data;
	}
}
