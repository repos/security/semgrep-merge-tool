# Semgrep Merge Tool

This is a simple tool to merge various, externally-hosted semgrep rules and policies for
packaging and consumption by a semgrep cli.  This tool is largely needed as an alternative
to the semgrep.dev/r rules/policies repository.

## Installation

1. Clone this repository
2. Install composer dependencies via `composer install`
3. Copy `config.example.php` to `config.php` and update any variables per your environment
4. Run `./util/SMTCli.php repos get` to build the cache directory and clone relevant semgrep rule repos
5. Run a PHP-capable web server to serve `index.php` as the default page
  * Note that for development purposes, `php -S localhost:8081` should be more than sufficient

## Usage

See [templates/index.twig.html](templates/index.twig.html) (preferably when being
served by a PHP-capable web server) for specific usage examples.

## Troubleshooting

1. Since this is a pretty simple web application, many issues can likely be debugged locally with standard versions of PHP and the contents of various error/exception logs.
2. However, this service is designed to run under Wikimedia's toolforge.org environment, and sometimes various issues occur there.  To try to get a quick sense of certain issues, viewing various log files might help, via: `webservice --backend=kubernetes logs`.  Another common issue might be using outdated images.  This can typically be fixed by viewing [the latest supported images](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Kubernetes#Available_container_types) and then upgrading the web service via:
```
$ webservice stop
$ webservice --backend=kubernetes php8.2 start
```

## Contributing

We openly welcome contributions to this project, from bug reports to new feature
development!  Please reach out to one of the maintaners or the Wikimedia Security Team's
general emails address [security-help@wikimedia.org](mailto:security-help@wikimedia.org)
if you would like to get involved or feel free to view our current project
board in Phabricator: [https://phabricator.wikimedia.org/project/profile/5984/](https://phabricator.wikimedia.org/project/profile/5984/)

## TODOs

1. Tests
2. Improve caching performance and overall architecture

## Authors

* [Manfredi Martorana](mailto:mmartorana@wikimedia.org)
* [Scott Bassett](mailto:sbassett@wikimedia.org)

## Version History

* 0.1
    * Initial beta release

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE](LICENSE) file for details.

## References

* [phabricator task](https://phabricator.wikimedia.org/T307962)
* [application security pipeline documentation](https://www.mediawiki.org/wiki/Security/Application_Security_Pipeline#Semgrep)
* [semgrep](https://semgrep.dev/)
