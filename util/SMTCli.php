#!/usr/bin/env php
<?php

/**
 * Copyright 2022 sbassett@wikimedia.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Maitenance script/cli largely for performing:
 *  1) shallow clones/deletes of relevant semgrep rules repos
 *  NOTE: expected to be run within util directory
 */

namespace Wikimedia\SemgrepMergeTool\Utils;

require_once __DIR__ . '/../vendor/autoload.php';

/* require config and dependencies */
use Wikimedia\SemgrepMergeTool\SMTCliClass;

/* run cli */
SMTCliClass::run();
