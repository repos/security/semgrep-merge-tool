<?php
/*
 * Copyright 2023 sguebo@wikimedia.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Wikimedia\SemgrepMergeTool\IndexPage;

#[CoversClass( IndexPage::class )]
class IndexPageTest extends \PHPUnit\Framework\TestCase {
	#[Test]
	// phpcs:ignore MediaWiki.Commenting.MissingCovers.MissingCovers
	public function testRenderThrowsExceptionIfConfigIsIncomplete() {
		$config = [];
		$this->expectException( Exception::class );
		IndexPage::render( $config );
	}

	#[Test]
	// phpcs:ignore MediaWiki.Commenting.MissingCovers.MissingCovers
	public function testRenderThrowsErrorIfTemplatesFolderIsInexistent() {
		$config = [ 'smt_template_dir' => '../dummy-folder' ];
		$this->expectException( Exception::class );
		IndexPage::render( $config );
	}

	#[Test]
	// phpcs:ignore MediaWiki.Commenting.MissingCovers.MissingCovers
	public function testRenderOutputHTMLString() {
		global $_SERVER;
		$_SERVER = [ 'HTTP_HOST' => '', 'PHP_SELF' => '' ];
		$config = $this->getDefaultConfig();
		$this->expectOutputRegex( "/<!doctype.+?>/" );
		IndexPage::render( $config );
	}

	private function getDefaultConfig(): array {
		return [
			'smt_template_dir' => 'templates',
			'smt_cache_dir' => 'cache',
			'smt_policies' => [],
			'smt_rule_repos' => []
		];
	}
}
