<?php
/*
 * Copyright 2023 sguebo@wikimedia.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

namespace Wikimedia\SemgrepMergeTool;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;

#[CoversClass( SMTCliClass::class )]
class SMTCliClassTest extends \PHPUnit\Framework\TestCase {
	use \phpmock\phpunit\PHPMock;

	private const SMT_CLI_SCRIPT = './SMTCli.php';

	#[Test]
	// phpcs:ignore MediaWiki.Commenting.MissingCovers.MissingCovers
	public function testCliHelpMenu() {
		$help_menu_output = "No action passed, which is required.\n\n";
		$help_menu_output .= "Usage: ./SMTCli.php {action}\n\n";
		$help_menu_output .= "Actions:\n\n";
		$help_menu_output .= "help          Show this help output\n";
		$help_menu_output .= "repos get     Get semgrep rule repos, as defined in config\n";
		$help_menu_output .= "clear hashes  Delete hash files within cache (repo dirs remain)";

		// test no action passed
		global $argv;
		$argv = [];
		$this->expectOutputString( $help_menu_output );
		SMTCliClass::run();

		$argv = [ self::SMT_CLI_SCRIPT, "help" ];
		$help_menu_output = "Usage: ./SMTCli.php {action}\n\n";
		$help_menu_output .= "Actions:\n\n";
		$help_menu_output .= "help          Show this help output\n";
		$help_menu_output .= "repos get     Get semgrep rule repos, as defined in config\n";
		$help_menu_output .= "clear hashes  Delete hash files within cache (repo dirs remain)";
		ob_clean();
		$this->expectOutputString( $help_menu_output );
		SMTCliClass::run();
	}

	#[Test]
	// phpcs:ignore MediaWiki.Commenting.MissingCovers.MissingCovers
	public function testRunThrowsExceptionWithIncorrectConfig() {
		$this->expectException( \Exception::class );
		SMTCliClass::run( [ 'invalid_data' ] );
	}

	#[Test]
	// phpcs:ignore MediaWiki.Commenting.MissingCovers.MissingCovers
	public function testReposGetWithBadRepoConfigData() {
		global $argv;
		$argv = [ self::SMT_CLI_SCRIPT, 'repos get' ];
		$config = [
			'smt_rule_repos' => 'invalid_data',
			'smt_cache_dir' => 'invalid_data',
		];
		$this->expectException( \Exception::class );
		SMTCliClass::run( $config );

		$config = [
			'smt_rule_repos' => [
				'test-repos' => [
					'repo' => 'ftp://gitlab.wikimedia.org/bad-example-12345',
					'version' => 'main',
					'subdirs' => [ 'a_rules_dir_12345' ]
				],
			],
			'smt_cache_dir' => 'invalid_data',
		];
		$this->expectException( \Exception::class );
		SMTCliClass::run( $config );
	}
}
