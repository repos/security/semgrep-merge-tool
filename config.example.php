<?php

/**
 * Copyright 2022 sbassett@wikimedia.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

return [
	'smt_cache_dir' => './cache',
	'smt_template_dir' => './templates',
	'smt_cache_ttl' => 2592000,
	'smt_rule_repos' => [
		'repos/security/wikimedia-semgrep-rules' => [
			'repo' => 'https://gitlab.wikimedia.org/repos/security/wikimedia-semgrep-rules.git',
			'version' => 'main',
			'subdirs' => [ 'http_leaks', 'javascript', 'mediawiki_authnz', 'php' ]
		],
		'elttam/semgrep-rules' => [
			'repo' => 'https://github.com/elttam/semgrep-rules.git',
			'version' => 'main',
			'subdirs' => [ 'rules/java' ]
		],
		'dgryski/semgrep-go' => [
			'repo' => 'https://github.com/dgryski/semgrep-go.git',
			'version' => 'master'
		],
		'trailofbits/semgrep-rules' => [
			'repo' => 'https://github.com/trailofbits/semgrep-rules.git',
			'version' => 'main',
			'subdirs' => [ 'go', 'python', 'rs' ]
		],
		'ajinabraham/njsscan' => [
			'repo' => 'https://github.com/ajinabraham/njsscan.git',
			'version' => 'tags/0.3.1',
			'subdirs' => [ 'njsscan/rules/semantic_grep' ]
		],
		'federicodotta/semgrep-rules' => [
			'repo' => 'https://github.com/federicodotta/semgrep-rules.git',
			'version' => 'main',
			'subdirs' => [ 'php/frameworks/yii', 'php/lang' ]
		]
	],
	'smt_exclude_rules' => [
		'trailofbits/semgrep-rules/go' => [
			'hanging-goroutine',
			'invalid-usage-of-modified-variable',
			'missing-runlock-on-rwmutex',
			'missing-unlock-before-return'
		]
	],
	'smt_policies' => [
		'php' => [
			'repos/security/wikimedia-semgrep-rules/php',
			'repos/security/wikimedia-semgrep-rules/mediawiki_authnz',
			'federicodotta/semgrep-rules/php/frameworks/yii',
			'federicodotta/semgrep-rules/php/lang'
		],
		'javascript' => [
			'repos/security/wikimedia-semgrep-rules/javascript',
			'ajinabraham/njsscan/njsscan/rules/semantic_grep'
		],
		'golang' => [
			'dgryski/semgrep-go',
			'trailofbits/semgrep-rules/go'
		],
		'python' => [
			'trailofbits/semgrep-rules/python'
		],
		'rust' => [
			'trailofbits/semgrep-rules/rs'
		],
		'java' => [
			'elttam/semgrep-rules/rules/java'
		]
	]
];
